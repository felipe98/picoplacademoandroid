package com.felipedeveloper.commons

import androidx.appcompat.widget.Toolbar

interface IToolbar {

    fun toolbarToLoad(toolbar: Toolbar)

    fun enableHomeDisplay(value: Boolean)

    fun toolbarTitle(title : String)
}