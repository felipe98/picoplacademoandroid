package com.felipedeveloper.picoplaca.listeners

interface BaseRecyclerClickListener<T> {

    fun onClick(type: T, position: Int)

}