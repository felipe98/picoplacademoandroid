package com.felipedeveloper.picoplaca.utils

import java.text.SimpleDateFormat
import java.util.*

class Utilities {

    companion object {

        var calendar: Calendar? = null

        var year = initializeCalendar()!!.get(Calendar.YEAR)
        var month = initializeCalendar()!!.get(Calendar.MONTH)
        var dayOfMonth = initializeCalendar()!!.get(Calendar.DAY_OF_MONTH)

        fun initializeCalendar(): Calendar? {
            return if (calendar == null) Calendar.getInstance()
            else calendar
        }

        fun formatDate(day: Int, month: Int, year: Int): String {
            val dayFormat: String = if (day < 10) "0$day" else day.toString()
            val monthFormat: String = if (month < 10) "0$month" else month.toString()
            return "$dayFormat/$monthFormat/$year"
        }

        fun formatTime(hour: Int, min: Int): String {
            val hourFormat: String = if (hour < 10) "0$hour" else hour.toString()
            val minFormat: String = if (min < 10) "0$min" else min.toString()
            return "$hourFormat:$minFormat"
        }

        var currentDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())
        var currentTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(Date())

        fun getRegisterDate(): String = "$currentDate      ${currentTime}"
    }
}