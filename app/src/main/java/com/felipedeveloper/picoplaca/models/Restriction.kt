package com.felipedeveloper.picoplaca.models

data class Restriction(var day: String, var code_one: Int, var code_two: Int )