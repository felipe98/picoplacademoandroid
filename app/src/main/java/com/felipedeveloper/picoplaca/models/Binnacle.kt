package com.felipedeveloper.picoplaca.models

data class Binnacle(
    var query_date: String? = null,
    var register_date: String? = null,
    var plete: String? = null,
    var contravention_counter: Int = 0,
    var isContravention: Boolean = false
)