package com.felipedeveloper.picoplaca.models

data class Counter(var count: Int = 0)