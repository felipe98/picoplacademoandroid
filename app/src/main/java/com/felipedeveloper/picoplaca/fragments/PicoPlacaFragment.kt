package com.felipedeveloper.picoplaca.fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.felipedeveloper.picoplaca.R
import com.felipedeveloper.picoplaca.models.Binnacle
import com.felipedeveloper.picoplaca.models.Counter
import com.felipedeveloper.picoplaca.models.Restriction
import com.felipedeveloper.picoplaca.utils.RxBus
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.dayOfMonth
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.formatDate
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.formatTime
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.getRegisterDate
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.initializeCalendar
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.month
import com.felipedeveloper.picoplaca.utils.Utilities.Companion.year
import com.felipedeveloper.commons.BaseFragment
import com.felipedeveloper.commons.inflate
import com.felipedeveloper.commons.toast
import java.util.*
import kotlin.collections.ArrayList

class PicoPlacaFragment : BaseFragment() {

    private val TIME_ONE = 7.0 // 7 AM
    private val TIME_TWO = 9.0 // 9 AM
    private val TIME_THREE = 16.0 // 4 PM
    private val TIME_FOUR = 19.30 // 7:30 PM

    private lateinit var et_plete: EditText
    private lateinit var et_date: EditText

    private var text_date: String? = null
    private var query_date: String? = null

    private var currentHour: Int = 0
    private var currentMin: Int = 0
    private var currentDay: String? = null
    private var restrictionDay: String? = null

    private var calendar: Calendar? = null

    private lateinit var btn_verify: Button

    private val restrictionsList: ArrayList<Restriction> by lazy { initializeRestriction() }

    override fun getLayoutResId(): Int {
        return R.layout.fragment_pico_placa
    }

    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = container?.inflate(getLayoutResId())

        et_plete = rootView!!.findViewById(R.id.et_plete)
        et_date = rootView.findViewById(R.id.et_date)
        btn_verify = rootView.findViewById(R.id.btn_verify)
        calendar = initializeCalendar()

        clickListeners()

        //EventBus que escucha en caso se obtenga contravención y countar representa la clase que almacena las cantidades de contravención
        RxBus.listen(Counter::class.java).subscribe {
            if (it.count == 0) {
                activity?.toast("Sí existe contravención")
            } else {
                activity?.toast("Sí existe contravención ${it.count}")
            }
        }

        return rootView
    }

    private fun clickListeners() {
        btn_verify.setOnClickListener {

            val plete = et_plete.text.toString()
            val date = et_date.text.toString()

            if (plete.isEmpty() || date.isEmpty()) {
                activity?.toast("Por favor, llene los campos requeridos")
                return@setOnClickListener
            }

            val lastValue = plete.substring(plete.length - 1)
            val currentTimeInMinuteFormat = ("$currentHour.$currentMin").toDouble()

            getDayByPleteEndValue(lastValue.toInt())


            when (currentDay) {

                restrictionDay -> {
                    if (isValidTime(currentHour.toDouble(), TIME_ONE, TIME_TWO) || isValidTime(currentTimeInMinuteFormat, TIME_THREE, TIME_FOUR)) {

                        RxBus.publish(Binnacle(query_date, getRegisterDate(), plete, 1, true))

                    } else {
                        showCancelMessage(plete)
                    }
                }

                else -> showCancelMessage(plete)

            }


        }

        et_date.setOnClickListener {
            datePicker()
        }
    }

    private fun showCancelMessage(plete: String) {
        activity?.toast("No existe contravención")
        RxBus.publish(Binnacle(query_date, getRegisterDate(), plete, 0, false))
    }


    private fun getDayByPleteEndValue(lastValue: Int) {
        restrictionsList.forEach {
            if (it.code_one == lastValue || it.code_two == lastValue) {
                restrictionDay = it.day
                return@forEach
            }
        }
    }

    private fun isValidTime(currentTime: Double, first: Double, second: Double): Boolean {
        return (currentTime in first..second)
    }

    private fun datePicker() {
        //year,month,dayOfMonth vienen de la clase Utilities, para ubicar el pickerDialog con la fecha actual
        val datePickerDialog = DatePickerDialog(
            activity!!, DatePickerDialog.OnDateSetListener { _, year, month, day ->
                currentDay = getNameOfDay(year, day)
                //Agregamos 1 porque el més empieza desde el índice 0
                text_date = formatDate(day, month + 1, year)
                timePicker()
            }, year, month, dayOfMonth
        )

        datePickerDialog.show()

    }

    private fun timePicker() {
        //Esteblecemos los valores 0 por defecto por que lo que nos importa es recibir el valor del TimePickerDialog
        val timePickerDialog = TimePickerDialog(
            activity!!, TimePickerDialog.OnTimeSetListener { _, hour, min ->
                currentHour = hour
                currentMin = min

                query_date = "$text_date      ${formatTime(hour, min)}"

                et_date.setText(query_date)

            }, 0, 0, false
        )
        timePickerDialog.show()
    }

    private fun getNameOfDay(year: Int, dayOfYear: Int): String {
        calendar?.set(Calendar.YEAR, year)
        calendar?.set(Calendar.DAY_OF_YEAR, dayOfYear)

        val days = arrayOf("Lunes", "Martes", "Miercoles", "Jueves", "Viernes")
        val dayIndex = calendar?.get(Calendar.DAY_OF_WEEK)

        return if (dayIndex in 2..6) days[calendar?.get(Calendar.DAY_OF_WEEK)!! - 2]
        else "Día no requerido"
    }


    private fun initializeRestriction(): ArrayList<Restriction> {
        return object : ArrayList<Restriction>() {
            init {
                add(Restriction("Lunes", 1, 2))
                add(Restriction("Martes", 3, 4))
                add(Restriction("Miercoles", 5, 6))
                add(Restriction("Jueves", 7, 8))
                add(Restriction("Viernes", 9, 0))
            }
        }
    }

}

