package com.felipedeveloper.picoplaca.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.felipedeveloper.picoplaca.R
import com.felipedeveloper.picoplaca.adapters.HistoryAdapter
import com.felipedeveloper.picoplaca.listeners.BaseRecyclerClickListener
import com.felipedeveloper.picoplaca.models.Binnacle
import com.felipedeveloper.picoplaca.utils.RxBus
import com.felipedeveloper.commons.BaseFragment
import com.felipedeveloper.commons.inflate

class BinnacleFragment : BaseFragment() {

    private lateinit var recycler: RecyclerView

    private lateinit var adapter: HistoryAdapter

    private val layoutManager by lazy { LinearLayoutManager(context) }

    private lateinit var list: ArrayList<Binnacle>

    override fun getLayoutResId(): Int {
        return R.layout.fragment_binnacle
    }

    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = container?.inflate(getLayoutResId())

        recycler = rootView!!.findViewById(R.id.recyclerView)
        list = ArrayList()

        setRecyclerView()

        RxBus.listen(Binnacle::class.java).subscribe {
            adapter.setData(it)
        }

        return rootView
    }

    private fun setRecyclerView() {
        recycler.setHasFixedSize(true)
        recycler.itemAnimator = DefaultItemAnimator()
        recycler.layoutManager = layoutManager
        adapter = (HistoryAdapter(list, object : BaseRecyclerClickListener<Binnacle> {
            override fun onClick(type: Binnacle, position: Int) {

            }
        }))
        recycler.adapter = adapter
    }

}
