package com.felipedeveloper.picoplaca.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.felipedeveloper.picoplaca.R
import com.felipedeveloper.picoplaca.models.Binnacle
import com.felipedeveloper.picoplaca.listeners.BaseRecyclerClickListener
import com.felipedeveloper.picoplaca.models.Counter
import com.felipedeveloper.picoplaca.utils.RxBus
import com.felipedeveloper.commons.inflate
import kotlinx.android.synthetic.main.lsv_item_binnacle.view.*

class HistoryAdapter(
    private var items: ArrayList<Binnacle>,
    private var clickListener: BaseRecyclerClickListener<Binnacle>
) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    fun setData(data: Binnacle) {
        items.add(data)
        verifyIfPleteExist(data.plete, data.isContravention)
        notifyDataSetChanged()
    }

    private fun verifyIfPleteExist(plete: String?, isContravention: Boolean) {
        var firstTime = false
        items.forEach {
            if (it.plete == plete && isContravention && !firstTime) {
                //publicar evento recuperando e incrementando su contravención
                RxBus.publish(Counter(it.contravention_counter++))
                firstTime = true
                return@forEach
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) = ViewHolder(parent.inflate(R.layout.lsv_item_binnacle))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], clickListener)

    override fun getItemCount(): Int = items.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(data: Binnacle, clickListener: BaseRecyclerClickListener<Binnacle>) = with(itemView) {

            plete.text = data.plete

            register_date.text = data.register_date

            query_date.text = data.query_date

            if (data.isContravention) contravention_status.text = "Si"
            else contravention_status.text = "No"

            //Click recycler events
            setOnClickListener { clickListener.onClick(data, adapterPosition) }

        }
    }

}